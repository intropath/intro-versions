# Versions

Public repo to set the minimum mobile app version that users should upgrade to.

# Edit and set new version

## Production

1. Go to https://gitlab.com/intropath/intro-versions/-/edit/master/bridge-production.json
2. Click on Edit
3. Change version
4. Click on `Commit Changes`

## Staging

1. Go to https://gitlab.com/intropath/intro-versions/-/edit/master/bridge-beta.json
2. Click on Edit
3. Change version
4. Click on `Commit Changes`

## Development

1. Go to https://gitlab.com/intropath/intro-versions/-/edit/master/bridge-development.json
2. Click on Edit
3. Change version
4. Click on `Commit Changes`
